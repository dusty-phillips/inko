# Types and methods for manipulating files on a filesystem.
#
# Rather than using a single "File" type for all different file modes, Inko uses
# three separate file types:
#
# - `ReadOnlyFile`: read-only file operations
# - `WriteOnlyFile`: write-only file operations
# - `ReadWriteFile`: read-write file operations
#
# Using different types per file mode allows for a type-safe file API.
#
# Files are automatically closed when they are dropped. Any errors that may
# occur when closing a file are ignored.
import std::drop::Drop
import std::fs::path::(IntoPath, Path)
import std::io::(Error, Read, Seek, Size, Write)
import std::string::ToString

# Removes the file for the given file path.
#
# # Examples
#
# Removing a file:
#
#     import std::fs::file::(self, WriteOnlyFile)
#
#     let handle = try! WriteOnlyFile.new('/tmp/test.txt')
#
#     try! handle.write('hello')
#     try! file.remove('/tmp/test.txt')
fn pub remove(path: ref ToString) !! Error {
  try _INKO.file_remove(path.to_string) else (e) throw Error.from_int(e)
}

# Copies a file from the source destination to the target destination,
# returning the number of copied bytes.
#
# # Examples
#
# Copying a file:
#
#     import std::fs::file::(self, WriteOnlyFile)
#
#     let handle = try! WriteOnlyFile.new('/tmp/test.txt')
#
#     try! handle.write('hello')
#     try! file.copy(from: '/tmp/test.txt', to: '/tmp/test2.txt')
fn pub copy(from: ref ToString, to: ref ToString) !! Error -> Int {
  try {
    _INKO.file_copy(from.to_string, to.to_string)
  } else (e) {
    throw Error.from_int(e)
  }
}

# A file that can only be used for reads.
class pub ReadOnlyFile {
  # The path of the file.
  let pub @path: Path

  # The internal file descriptor.
  let @fd: Any

  # Returns a new `ReadOnlyFile`.
  #
  # # Examples
  #
  # Opening a file in read-only mode:
  #
  #     import std::fs::file::ReadOnlyFile
  #
  #     let handle = try! ReadOnlyFile.new('/dev/null')
  fn pub static new(path: IntoPath) !! Error -> Self {
    let path_obj = path.into_path
    let fd = try {
      _INKO.file_open_read_only(path_obj.to_string)
    } else (e) {
      throw Error.from_int(e)
    }

    Self { @path = path_obj, @fd = fd }
  }
}

impl Drop for ReadOnlyFile {
  fn mut drop {
    _INKO.file_drop(@fd)
  }
}

impl Read for ReadOnlyFile {
  fn pub mut read(into: mut ByteArray, size: Int) !! Error -> Int {
    try _INKO.file_read(@fd, into, size) else (e) throw Error.from_int(e)
  }
}

impl Seek for ReadOnlyFile {
  fn pub mut seek(position: Int) !! Error -> Int {
    try _INKO.file_seek(@fd, position) else (err) throw Error.from_int(err)
  }
}

impl Size for ReadOnlyFile {
  fn pub size !! Error -> Int {
    try _INKO.file_size(@path.to_string) else (err) throw Error.from_int(err)
  }
}

# A file that can only be used for writes.
class pub WriteOnlyFile {
  # The path of the file.
  let pub @path: Path

  # The internal file descriptor.
  let @fd: Any

  # Opens a file in write-only mode.
  #
  # # Examples
  #
  #     import std::fs::file::WriteOnlyFile
  #
  #     let file = try! WriteOnlyFile.new('/dev/null')
  fn pub static new(path: IntoPath) !! Error -> Self {
    let path_obj = path.into_path
    let fd = try {
      _INKO.file_open_write_only(path_obj.to_string)
    } else (e) {
      throw Error.from_int(e)
    }

    Self { @path = path_obj, @fd = fd }
  }

  # Opens a file in append-only mode.
  #
  # # Examples
  #
  #     import std::fs::file::WriteOnlyFile
  #
  #     let file = try! WriteOnlyFile.append('/dev/null')
  fn pub static append(path: IntoPath) !! Error -> Self {
    let path_obj = path.into_path
    let fd = try {
      _INKO.file_open_append_only(path_obj.to_string)
    } else (e) {
      throw Error.from_int(e)
    }

    Self { @path = path_obj, @fd = fd }
  }
}

impl Drop for WriteOnlyFile {
  fn mut drop {
    _INKO.file_drop(@fd)
  }
}

impl Write for WriteOnlyFile {
  fn pub mut write_bytes(bytes: ref ByteArray) !! Error -> Int {
    try _INKO.file_write_bytes(@fd, bytes) else (e) throw Error.from_int(e)
  }

  fn pub mut write_string(string: String) !! Error -> Int {
    try _INKO.file_write_string(@fd, string) else (e) throw Error.from_int(e)
  }

  fn pub mut flush !! Error {
    try _INKO.file_flush(@fd) else (e) throw Error.from_int(e)
  }
}

impl Seek for WriteOnlyFile {
  fn pub mut seek(position: Int) !! Error -> Int {
    try _INKO.file_seek(@fd, position) else (e) throw Error.from_int(e)
  }
}

# A file that can be used for both reads and writes.
class pub ReadWriteFile {
  # The path of the file.
  let pub @path: Path

  # The internal file descriptor.
  let @fd: Any

  # Opens a file for both reading and writing:
  #
  # # Examples
  #
  #     import std::fs::file::ReadWriteFile
  #
  #     let handle = try! ReadWriteFile.new('/dev/null')
  fn pub static new(path: IntoPath) !! Error -> Self {
    let path_obj = path.into_path
    let fd = try {
      _INKO.file_open_read_write(path_obj.to_string)
    } else (e) {
      throw Error.from_int(e)
    }

    Self { @path = path_obj, @fd = fd }
  }

  # Opens a file for both reading and appending:
  #
  # # Examples
  #
  #     import std::fs::file::ReadWriteFile
  #
  #     let handle = try! ReadWriteFile.append('/dev/null')
  fn pub static append(path: IntoPath) !! Error -> Self {
    let path_obj = path.into_path
    let fd = try {
      _INKO.file_open_read_append(path_obj.to_string)
    } else (e) {
      throw Error.from_int(e)
    }

    Self { @path = path_obj, @fd = fd }
  }
}

impl Drop for ReadWriteFile {
  fn mut drop {
    _INKO.file_drop(@fd)
  }
}

impl Read for ReadWriteFile {
  fn pub mut read(into: mut ByteArray, size: Int) !! Error -> Int {
    try _INKO.file_read(@fd, into, size) else (e) throw Error.from_int(e)
  }
}

impl Write for ReadWriteFile {
  fn pub mut write_bytes(bytes: ref ByteArray) !! Error -> Int {
    try _INKO.file_write_bytes(@fd, bytes) else (e) throw Error.from_int(e)
  }

  fn pub mut write_string(string: String) !! Error -> Int {
    try _INKO.file_write_string(@fd, string) else (e) throw Error.from_int(e)
  }

  fn pub mut flush !! Error {
    try _INKO.file_flush(@fd) else (err) throw Error.from_int(err)
  }
}

impl Seek for ReadWriteFile {
  fn pub mut seek(position: Int) !! Error -> Int {
    try _INKO.file_seek(@fd, position) else (e) throw Error.from_int(e)
  }
}

impl Size for ReadWriteFile {
  fn pub size !! Error -> Int {
    try _INKO.file_size(@path.to_string) else (e) throw Error.from_int(e)
  }
}
